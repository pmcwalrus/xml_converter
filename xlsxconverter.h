#ifndef XLSXCONVERTER_H
#define XLSXCONVERTER_H

#include <QObject>
#include <QString>
#include <QAxObject>
#include <QFile>
#include <QTextStream>
#include <QCharRef>
#include <QVariant>

/* Возвращаемые коды */
#define CONVERTER_OK    0xFFFF
#define CONVERTER_ERROR 0xFFFA

/* Кодировка цветов */
#define BLACK   "black"
#define RED     "red"
#define GREEN   "green"
#define BLUE    "blue"

/* Кодировки названий */
#define XML_FILE_NAME   "output.xml"

class XlsxConverter : public QObject
{
    Q_OBJECT
public:
    /* Структура навигации */
    struct navigation_struct{
        unsigned int    param_index_column;     // Стобец "Индекс Параметра"
        unsigned int    param_name_column;      // Столбец "Наименование параметра"
        unsigned int    system_id_column;       // Столбец "SystemID"
        unsigned int    signal_id_column;       // Столбец "SignalID"
        unsigned int    alarm_column;          // Столбец "Сигнализация"
        unsigned int    type_column;            // "Type"
        unsigned int    signal_type_column;     // "Тип сигнала"
        unsigned int    inversion_column;       // "Инверсия"
        unsigned int    page_1_head_row;        // Строка с заголовками на тсранице 1
        unsigned int    bus_column;
    };

    /* Методы */
    explicit XlsxConverter(QObject *parent = nullptr);
    unsigned int ConvertXlsxToXml(QString excel_path);     // Основной обработчик
    unsigned int OpenXlsxFile(QString excel_path);         // Открытие файла excel
    unsigned int CreateXmlFile(void);                      // Создание файла XML
    void CloseXlsx(void);                                   // Закрытие файла excel
    void GenerateXmlHeader(void);                           // Запись заголовка XML
    void GenerateShmemHeader(void);
    int FindParameter(QString index);                       // Поиск строки в перечне с указанным индексом параметра
    unsigned int FillNavigationStruct(struct navigation_struct * navigation);   // Заполнение наигационной структуры
    unsigned int GenerateNscDev(struct navigation_struct * navigation);         // Генерирование блока nsc-dev
    unsigned int FillNscDevPvv(struct navigation_struct * navigation, unsigned int pvv_num,
                               unsigned int first_signal_row, unsigned int first_signal_column);
    unsigned int FillNscDevSignal(struct navigation_struct * navigation, QString index);
    unsigned int FillNscDevAlg(struct navigation_struct * navigation, QString shmem);
    unsigned int FillAddrAlg(struct navigation_struct * navigation, QString pvv);
    unsigned int GenerateAdresses(struct navigation_struct * navigation);
    unsigned int FillAdressBlock(struct navigation_struct * navigation, unsigned int pvv_num,
                                              unsigned int first_signal_row, unsigned int first_signal_column);
    unsigned int FillSignalAdress(struct navigation_struct * navigation, QString signal_index, int modbus_function, int signal_address);
    unsigned int GenerateConnections(struct navigation_struct * navigation);
    unsigned int GenerateWorkstations(struct navigation_struct * navigation);
    unsigned int CheckParams(struct navigation_struct * navigation);
    unsigned int GeneratePus(struct navigation_struct * navigation);
    unsigned int FillPus(struct navigation_struct * navigation);
    unsigned int WriteSetpointStrings(struct navigation_struct * navigation, int row, QString param_name);
    unsigned int GenerateAlgstatic(struct navigation_struct * navigation);
    unsigned int GenerateUPS(struct navigation_struct * navigation);
    unsigned int GenerateSK(struct navigation_struct * navigation);
    unsigned int GenerateVK(struct navigation_struct * navigation);
    unsigned int GenerateKDMP(struct navigation_struct * navigation);

private:

    /* Объекты для открытия и хранения данных таблицы Excel */
    QAxObject* excel_file;          // Указатель на приложение Excel
    QAxObject* excel_workbooks;     // Уазатель на workbooks
    QAxObject* excel_workbook;      // Указатель на сам файл таблицы
    QAxObject* excel_sheets;        // Указатель на перечень листов
    QAxObject* excel_sheet_1;       // Указатель на первый лист таблицы
    QAxObject* excel_sheet_2;       // Указатель на второй лист таблицы
    QAxObject* excel_sheet_3;       // Указатель на третий лист таблицы
    QAxObject* excel_sheet_7;       // Указатель на лист таблицы IP
    QAxObject* excel_sheet_pus;       // Указатель на лист таблицы PUS
    QAxObject* excel_sheet_ups;       // Указатель на лист таблицы УПС
    QAxObject* excel_sheet_alg;       // Указатель на лист таблицы алгоритм
    QAxObject* excel_sheet_vk;       // Указатель на лист таблицы ВК
    QAxObject* excel_cell;          // Указатель на клетку
    QAxObject* excel_used_range;
    QAxObject* excel_columns;
    QAxObject* excel_rows;
    /*------------------------------------------------------*/

    /* Объекты для открытия и редактирования данных файла XML */
    QFile *xml_file;
    /*--------------------------------------------------------*/

    /* Отладочные переменные */
    QString debug_string;           // Строка для вывода отладочной информации
    /*-----------------------*/

    /* Навигационная переменные */
    int rows_count_sheet_1 = 0;     // Количсетво строк на первой странице
    int columns_count_sheet_1 = 0;     // Количсетво столбцов на первой странице
    int rows_count_sheet_2 = 0;     // Количсетво строк на второй странице
    int columns_count_sheet_2 = 0;     // Количсетво столбцов на второй странице
    int rows_count_sheet_3 = 0;     // Количсетво строк на третьей странице
    int columns_count_sheet_3 = 0;     // Количсетво столбцов на третьей странице
    int rows_count_sheet_7 = 0;     // Количсетво строк на седьмой странице
    int columns_count_sheet_7 = 0;     // Количсетво столбцов на седьмой странице
    int rows_count_sheet_pus = 0;     // Количсетво строк на восьмой странице
    int columns_count_sheet_pus = 0;     // Количсетво столбцов на восьмой странице
    int rows_count_sheet_alg = 0;     // Количсетво строк на девятой странице
    int columns_count_sheet_alg = 0;     // Количсетво столбцов на девятой странице
    int rows_count_sheet_ups = 0;     // Количсетво строк на девятой странице
    int columns_count_sheet_ups = 0;     // Количсетво столбцов на девятой странице
    int rows_count_sheet_vk = 0;     // Количсетво строк на  странице ВК
    int columns_count_sheet_vk = 0;     // Количсетво столбцов на  странице ВК
    int pvv_column = 1;             // Столбец с номерами ПВВ



signals:
    void debug_output(QString str, QString color);

public slots:
};

#endif // XLSXCONVERTER_H
