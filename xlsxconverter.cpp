#include "xlsxconverter.h"

XlsxConverter::XlsxConverter(QObject *parent) : QObject(parent)
{

}

/* Обработчик конвертера */
unsigned int XlsxConverter::ConvertXlsxToXml(QString excel_path) {

    /* Открываем файл таблицы Excel */
    if (OpenXlsxFile(excel_path) != CONVERTER_OK) {
        emit debug_output("Работа программы завершилась с ошибкой!\n", RED);
        /* Закрываем таблицу */
        CloseXlsx();
        return CONVERTER_ERROR;
    }

    /* Создаем навигационную структуру */
    struct navigation_struct navigation;

    if (FillNavigationStruct(&navigation) != CONVERTER_OK) {
        emit debug_output("Работа программы завершилась с ошибкой!\n", RED);
        /* Закрываем таблицу */
        CloseXlsx();
        return CONVERTER_ERROR;
    }

    /* Создаем файл XML */
    if (CreateXmlFile() != CONVERTER_OK) {
        emit debug_output("Работа программы завершилась с ошибкой!\n", RED);
        /* Закрываем таблицу */
        CloseXlsx();
        return CONVERTER_ERROR;
    }

    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");

    CheckParams(&navigation);
    /* Генерируем заголовок XML файла */
    GenerateXmlHeader();
    /* Генерируем заголовочный блок shmem */
    GenerateShmemHeader();

    /* Генерируем блок nsc-dev */
    GenerateNscDev(&navigation);

    /* Генерируем smem'ы для ПУС */
    FillPus(&navigation);

    /* Генерируем секцмю algstatic */
    GenerateAlgstatic(&navigation);

    /* Генерируем подключения */
    GenerateConnections(&navigation);

    /* Генерируем блоки с адресами */
    GenerateAdresses(&navigation);

    /* Генерируем блоки с адресами */
    GenerateWorkstations(&navigation);
    if (GeneratePus(&navigation) != CONVERTER_OK) {
        emit debug_output("Работа программы завершилась с ошибкой!\n", RED);
        /* Закрываем таблицу */
        CloseXlsx();
        return CONVERTER_ERROR;
    }

    xml_stream << endl << "</nsc-sys>"  << endl;


    /* Закрываем таблицу */
    CloseXlsx();
    emit debug_output("Файл успешно сгенерирован!\n", GREEN);

    return CONVERTER_OK;
}

/* Открытие файла Excel */
unsigned int XlsxConverter::OpenXlsxFile(QString excel_path) {
    emit debug_output("Открытие файла... ", BLACK);
    excel_file = new QAxObject("Excel.Application", nullptr);
    excel_workbooks = excel_file->querySubObject("Workbooks");
    excel_workbook = excel_workbooks->querySubObject("Open(const QString&)", excel_path);
    if (excel_workbook != nullptr) {
        emit debug_output("Успешно! \n", GREEN);
        excel_sheets = excel_workbook->querySubObject("Worksheets");

        emit debug_output("Открытие листа 1 (\"Перечень\")... ", BLACK);
        excel_sheet_1 = excel_sheets->querySubObject("Item(int)", 1);
        if (excel_sheet_1 == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 2 (\"Раскладка\")... ", BLACK);
        excel_sheet_2 = excel_sheets->querySubObject("Item(int)", 2);
        if (excel_sheet_2 == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 3 (\"ОАПС\")... ", BLACK);
        excel_sheet_3 = excel_sheets->querySubObject("Item(int)", 3);
        if (excel_sheet_3 == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 7 (\"IP\")... ", BLACK);
        excel_sheet_7 = excel_sheets->querySubObject("Item(int)", 7);
        if (excel_sheet_7 == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 8 (\"ПУС\")... ", BLACK);
        excel_sheet_pus = excel_sheets->querySubObject("Item(int)", 8);
        if (excel_sheet_pus == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 9 (\"Алгоритмы\")... ", BLACK);
        excel_sheet_alg = excel_sheets->querySubObject("Item(int)", 9);
        if (excel_sheet_alg == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 3 (\"УПС\")... ", BLACK);
        excel_sheet_ups = excel_sheets->querySubObject("Item(int)", 3);
        if (excel_sheet_ups == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        emit debug_output("Открытие листа 4 (\"ВК\")... ", BLACK);
        excel_sheet_vk = excel_sheets->querySubObject("Item(int)", 4);
        if (excel_sheet_vk == nullptr) {
            emit debug_output("Ошибка!\n", RED);
            return CONVERTER_ERROR;
        }
        emit debug_output("Успешно! \n", GREEN);

        /* Определяем количество строк на первой странице */
        emit debug_output("Количество строк на странице 1: ", BLACK);
        rows_count_sheet_1 = excel_sheet_1->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_1) + "\n", BLUE);

        /* Определяем количество столбцов на первой странице */
        emit debug_output("Количество столбцов на странице 1: ", BLACK);
        //columns_count_sheet_1 = excel_sheet_1->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        columns_count_sheet_1 = 34;
        emit debug_output(QString::number(columns_count_sheet_1) + "\n", BLUE);

        /* Определяем количество строк на второй странице */
        emit debug_output("Количество строк на странице 2: ", BLACK);
        rows_count_sheet_2 = excel_sheet_2->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_2) + "\n", BLUE);

        /* Определяем количество столбцов на второй странице */
        emit debug_output("Количество столбцов на странице 2: ", BLACK);
        columns_count_sheet_2 = excel_sheet_2->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_2) + "\n", BLUE);

        /* Определяем количество строк на третьей странице */
        emit debug_output("Количество строк на странице 3: ", BLACK);
        rows_count_sheet_3 = excel_sheet_3->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_3) + "\n", BLUE);

        /* Определяем количество столбцов на третьей странице */
        emit debug_output("Количество столбцов на странице 3: ", BLACK);
        columns_count_sheet_3 = excel_sheet_3->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_3) + "\n", BLUE);

        /* Определяем количество строк на седьмой странице */
        emit debug_output("Количество строк на странице 7: ", BLACK);
        rows_count_sheet_7 = excel_sheet_7->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_7) + "\n", BLUE);

        /* Определяем количество столбцов на седьмой странице */
        emit debug_output("Количество столбцов на странице 7: ", BLACK);
        columns_count_sheet_7 = excel_sheet_7->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_7) + "\n", BLUE);

        /* Определяем количество столбцов на странице PUS*/
        emit debug_output("Количество строк на странице ПУС: ", BLACK);
        rows_count_sheet_pus = excel_sheet_pus->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_pus) + "\n", BLUE);

        /* Определяем количество столбцов на седьмой ПУС */
        emit debug_output("Количество столбцов на странице ПУС: ", BLACK);
        columns_count_sheet_pus = excel_sheet_pus->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_pus) + "\n", BLUE);

        /* Определяем количество столбцов на странице Алгоритмы*/
        emit debug_output("Количество строк на странице Алгоритмы: ", BLACK);
        rows_count_sheet_alg = excel_sheet_alg->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_alg) + "\n", BLUE);

        /* Определяем количество столбцов на странице Алгоритмы*/
        emit debug_output("Количество столбцов на странице Алгоритмы: ", BLACK);
        columns_count_sheet_alg = excel_sheet_alg->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_alg) + "\n", BLUE);

        /* Определяем количество столбцов на странице УПС*/
        emit debug_output("Количество строк на странице УПС: ", BLACK);
        rows_count_sheet_ups = excel_sheet_ups->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_ups) + "\n", BLUE);

        /* Определяем количество столбцов на странице УПС*/
        emit debug_output("Количество столбцов на странице УПС: ", BLACK);
        columns_count_sheet_ups = excel_sheet_ups->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_ups) + "\n", BLUE);

        /* Определяем количество столбцов на странице ВК*/
        emit debug_output("Количество строк на странице ВК: ", BLACK);
        rows_count_sheet_vk = excel_sheet_vk->querySubObject("UsedRange")->querySubObject("Rows")->property("Count").toInt();
        emit debug_output(QString::number(rows_count_sheet_vk) + "\n", BLUE);

        /* Определяем количество столбцов на странице ВК*/
        emit debug_output("Количество столбцов на странице ВК: ", BLACK);
        columns_count_sheet_vk = excel_sheet_vk->querySubObject("UsedRange")->querySubObject("Columns")->property("Count").toInt();
        emit debug_output(QString::number(columns_count_sheet_vk) + "\n", BLUE);

        excel_cell = excel_sheet_1->querySubObject("Cells(int,int)", 1, 1);
        return CONVERTER_OK;
    } else {
        emit debug_output("Ошибка! \nНевозможно открыть указанный файл!\n", RED);
        return CONVERTER_ERROR;
    }
}

/* Создание XML-файла */
unsigned int XlsxConverter::CreateXmlFile (void) {
    emit debug_output("Создаем файл XML... ", BLACK);
    xml_file = new QFile(XML_FILE_NAME);
    if (xml_file->open(QIODevice::WriteOnly)) {
        emit debug_output("Успешно! \n", GREEN);
    } else {
        emit debug_output("Ошибка!\n", RED);
    }
    return CONVERTER_OK;
}

void XlsxConverter::CloseXlsx(void) {
    excel_workbook->dynamicCall("Close()");
    excel_file->dynamicCall("Quit()");
}

/* Генератор заголовка XML-файла */
void XlsxConverter::GenerateXmlHeader(void) {
    emit debug_output("Генерирурем заголовок XML... ", BLACK);
    int version = 10;           // Версия XML
    int nsc_version = 1;        // Версия nsc_sys
    QString nsc_name = "dev";   // Название nsc_sys
    QString encoding = "UTF-8"; // Кодировка
    QString result_string;
    /* Генерируем первую строку */
    result_string = "<?xml version=\"";
    result_string += QString::number(version/10) + "." + QString::number(version % 10) + "\" ";
    result_string += "encoding=\"" + encoding + "\" ?>";
    /* Выводим первую строку */
    QTextStream xml_stream(xml_file);
    xml_stream << result_string << endl << endl;
    emit debug_output("Успешно! \n", GREEN);
    /* Генерируем заголовок блока nsc-sys */
    emit debug_output("Открываем блок nsc-sys... ", BLACK);
    result_string = "<nsc-sys name=\"";
    result_string += nsc_name + "\"";
    result_string += " version=\"" + QString::number(nsc_version/10) + "." + QString::number(nsc_version % 10) + "\">";
    xml_stream << result_string << endl << endl;
    emit debug_output("Успешно! \n", GREEN);
}

/* Генератор заголовка блоков shmem */
void XlsxConverter::GenerateShmemHeader(void) {
    emit debug_output("Генерирурем заголовок блоки shmem:\n ", BLACK);
    QTextStream xml_stream(xml_file);
    QString result_string;
    QString sript_path = "scripts/init.qs";
    /* Записываем путь к скрипту */
    result_string = "\t<script file=\"" + sript_path + "\"/>";
    xml_stream << result_string << endl << endl;
    /* Прописываем статичную информацию */
    result_string = "\t<database name=\"nsc\"/>";
    xml_stream << result_string << endl;
    result_string = "\t<!--<database name=\"nsc\" host=\"192.168.58.12\"/>-->";
    xml_stream << result_string << endl << endl;
    result_string = "\t<shmem name=\"nsc-dev-ex3\" type=\"exclusive\"/>";
    xml_stream << result_string << endl;
    result_string = "\t<shmem name=\"sys-ws\" type=\"local\" keepalive=\"true\">";
    xml_stream << result_string << endl;
    result_string = "\t\t<parm name=\"SYS_ws_status\" type=\"int\"/>";
    xml_stream << result_string << endl;
    result_string = "\t\t<parm name=\"SYS_db_connection\" type=\"int\"/>";
    xml_stream << result_string << endl;
    result_string = "\t</shmem>";
    xml_stream << result_string << endl << endl;

}



/* Поиск координат параметра, относительно его индекса */
int XlsxConverter::FindParameter(QString index) {
    unsigned int index_column = 3;
    unsigned int index_row = 1;
    QVariant read_value;
    QString return_value;
    while ((index_row <= rows_count_sheet_1) && (index != return_value)) {
        read_value = excel_sheet_1->querySubObject("Cells(int,int)",index_row, index_column)->property("Value");
        return_value = read_value.toString();
        ++ index_row;
    }
    if (return_value != index) {
        return -1;
    } else {
        return (index_row - 1);
    }
}

/* Заполнение навигационной структуры */
unsigned int XlsxConverter::FillNavigationStruct(struct navigation_struct * navigation) {
    unsigned int column = 1;
    QString read_data;
    navigation->page_1_head_row = 1;    // Задаем строку с заголовками таблицы
    navigation->param_index_column = 3; // Задаем столбец "Индекс параметра"
    navigation->param_name_column = 4;  // "Наименование параметра"
    navigation->signal_id_column = 16;  // SignalId
    navigation->system_id_column = 15;  // SystemId
    navigation->alarm_column = 12;      // "Сигнализация"
    navigation->type_column = 17;      // "Type"
    navigation->signal_type_column = 14;      // "Тип сигнала"
    navigation->inversion_column = 13;      // "Инверсия"
    navigation->bus_column = 23;

    return CONVERTER_OK;
}

/* Генерирование блока nsc-dev */
unsigned int XlsxConverter::GenerateNscDev(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    QString result_string;
    QString read_value;
    unsigned int pvv_num = 0;       // Номер ПВВ
    unsigned int pvv_column = 1;    // Столбец с названиями ПВВ в раскладке
    unsigned int pvv_row;           // Строка на листе ракладки

    emit debug_output("Генерируем блок nsc-dev:\n", BLACK);

//    /* Пишем заголовок */
//    result_string = "\t<shmem name=\"nsc-dev\">";
//    xml_stream << result_string <<endl;

    /* Генерируем блок для указанных ПВВ (должно стоять $$_*номер ПВВ) */
    for (pvv_row = 1; pvv_row <= rows_count_sheet_2; ++pvv_row) {
        /* Читаем значение в ячейке */
        read_value = excel_sheet_2->querySubObject("Cells(int,int)", pvv_row, pvv_column)->property("Value").toString();
        /* Определяем, нужно ли генерировать сигналы для этого ПВВ */
        if(read_value.indexOf("$$_") != -1) {
            pvv_num = read_value[read_value.indexOf("$$_") + 3].digitValue();
            emit debug_output("\tПВВ №" + QString::number(pvv_num) + ":\n", BLACK);
            FillNscDevPvv(navigation, pvv_num, pvv_row + 2, pvv_column + 1);

        }
    }

//    /* Закрываем блок nsc-dev */
//    result_string = "\t</shmem>";
//    xml_stream << result_string <<endl << endl;
    return  0;
}

unsigned int XlsxConverter::FillNscDevPvv(struct navigation_struct * navigation, unsigned int pvv_num,
                                          unsigned int first_signal_row, unsigned int first_signal_column) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    unsigned int module_count;
    int module_number;
    unsigned int signal_number;
    QString module_name;
    QString module_description;
    QString module_type;
    QString signal_index;
    QString signals_type;
    QAxObject*  find_range;
    bool di_flag = false;
    bool do_flag = false;
    bool ai_flag = false;
    bool ao_flag = false;
    bool ptx_flag = false;

    /* Пишем заголовок для блока с параметрами связи */
    result_string = "\t<shmem name=\"nsc-dev" + QString::number(pvv_num) + "\">";
    xml_stream << result_string <<endl;

    /* Генерируем параметры связи */
    result_string = "\t\t<parm name=\"DIO_" + QString::number(pvv_num) + "_FCM1\" description=\"DIO_" +QString::number(pvv_num);
    result_string += " статус используемого FCM-MTCP модуля\" type=\"int\"/>\n";
    result_string += "\t\t<parm name=\"DIO_" + QString::number(pvv_num) + "_FCM2\" description=\"DIO_" +QString::number(pvv_num);
    result_string += " статус резервного FCM-MTCP модуля\" type=\"int\"/>\n";
    result_string += "\t\t<parm name=\"DIO_" + QString::number(pvv_num) + "_FPM\" description=\"DIO_" +QString::number(pvv_num);
    result_string += " статус модулей питания\" type=\"int\"/>\n";
    xml_stream << result_string << endl;

    /* Генерируем параметры модулей */
    for(module_count = 1; module_count <= 8; ++module_count) {
        if(!(excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 1, first_signal_column + module_count - 1)->property("Value").toString().isEmpty())) {
            module_number = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 1, first_signal_column + module_count - 1)->property("Value").toInt();
            module_name = "\"DIO_" + QString::number(pvv_num) +"_M" + QString::number(module_number) +"\"";
            module_description = "\"DIO_" + QString::number(pvv_num) + " статус модуля " + QString::number(module_number) +"\"";
            module_type = "\"int\"";
            result_string = "\t\t<parm name=" + module_name + " description=" + module_description + " type="+ module_type + "/>";
            xml_stream << result_string << endl;
        }
    }
    xml_stream << endl;

    /* Закрываем блок nsc-dev */
    result_string = "\t</shmem>";
    xml_stream << result_string <<endl << endl;

    /* Генерируем сигналы */
    /* Сначала блоки DI */
    for (module_count = 0; module_count < 8; ++module_count) {
        signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
        if (signals_type.contains("DI")) {
            di_flag = true;
        }
    }

    if (di_flag) {
        result_string = "\t<shmem name=\"nsc-dev" + QString::number(pvv_num) + "-DI\"" + " stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (module_count = 0; module_count < 8; ++module_count) {
            signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
            if (signals_type.contains("DI")) {
                for (signal_number = 0; signal_number < 32; ++signal_number) {
                    signal_index =excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row + signal_number, first_signal_column + module_count)->property("Value").toString();
                    if(!signal_index.isEmpty()) {
                        FillNscDevSignal(navigation, signal_index);
                    }
                }
                FillNscDevAlg(navigation, "nsc-dev" + QString::number(pvv_num) + "-DI");
            }
        }
        /* Закрываем блок nsc-dev */
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl << endl;
    }

    /* Блоки DO */
    for (module_count = 0; module_count < 8; ++module_count) {
        signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
        if (signals_type.contains("DO")) {
            do_flag = true;
        }
    }

    if(do_flag) {
        result_string = "\t<shmem name=\"nsc-dev" + QString::number(pvv_num) + "-DO\"" + " stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (module_count = 0; module_count < 8; ++module_count) {
            signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
            if (signals_type.contains("DO")) {
                for (signal_number = 0; signal_number < 32; ++signal_number) {
                    signal_index =excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row + signal_number, first_signal_column + module_count)->property("Value").toString();
                    if(!signal_index.isEmpty()) {
                        FillNscDevSignal(navigation, signal_index);
                    }
                }
                FillNscDevAlg(navigation, "nsc-dev" + QString::number(pvv_num) + "-DO");
            }
        }
        /* Закрываем блок nsc-dev */
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl << endl;
    }


    /* Блоки AI */
    for (module_count = 0; module_count < 8; ++module_count) {
        signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
        if (signals_type.contains("AI")) {
            ai_flag = true;
        }
    }

    if(ai_flag) {
        result_string = "\t<shmem name=\"nsc-dev" + QString::number(pvv_num) + "-AI\"" + " stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (module_count = 0; module_count < 8; ++module_count) {
            signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
            if (signals_type.contains("AI")) {
                for (signal_number = 0; signal_number < 32; ++signal_number) {
                    signal_index =excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row + signal_number, first_signal_column + module_count)->property("Value").toString();
                    if(!signal_index.isEmpty()) {
                        FillNscDevSignal(navigation, signal_index);
                    }
                }
                FillNscDevAlg(navigation, "nsc-dev" + QString::number(pvv_num) + "-AI");
            }
        }
        /* Закрываем блок nsc-dev */
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl << endl;
    }

    /* Блоки Ptx */
    for (module_count = 0; module_count < 8; ++module_count) {
        signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
        if (signals_type.contains("Ptx")) {
            ptx_flag = true;
        }
    }

    if (ptx_flag) {
        result_string = "\t<shmem name=\"nsc-dev" + QString::number(pvv_num) + "-Ptx\"" + " stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (module_count = 0; module_count < 8; ++module_count) {
            signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
            if (signals_type.contains("Ptx")) {
                for (signal_number = 0; signal_number < 32; ++signal_number) {
                    signal_index =excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row + signal_number, first_signal_column + module_count)->property("Value").toString();
                    if(!signal_index.isEmpty()) {
                        FillNscDevSignal(navigation, signal_index);
                    }
                }
                FillNscDevAlg(navigation, "nsc-dev" + QString::number(pvv_num) + "-Ptx");
            }
        }
        /* Закрываем блок nsc-dev */
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl << endl;
    }


    xml_stream << endl;
    return  0;
}



/* Добавление сигналов в nsc-dev */
unsigned int XlsxConverter::FillNscDevSignal(struct navigation_struct * navigation, QString index) {
    QTextStream xml_stream(xml_file);
    QVariant index_var = index;
    xml_stream.setCodec("UTF-8");
    QAxObject* find_range;
    QString result_string;
    QString name;
    QString type;
    QString inverted;
    QString signal_id;
    QString system_id;
    QString alarm;
    QString type_value;
    QString signal_type_value;
    QString inversion_value;
    QString description;
    int parameter_row = 1;

    /* Ищем индекс в перечне */
    find_range = excel_sheet_1->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", index);
    if (find_range != nullptr) {
        parameter_row = find_range->property("Row").toInt();
    } else {
        emit debug_output("\tСигнал с индексом " + index + " отсутствует в перечне!\n", RED);
        return CONVERTER_ERROR;
    }


    /* Определяем инверсию */
    inversion_value = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->inversion_column)->property("Value").toString();
    if (inversion_value == "+") {
        inverted = "\"true\"";
    }

    /* Определяем тип сигнала */
    type_value = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->type_column)->property("Value").toString();
    if (type_value == "AI") {
        type = "\"float\"";
    } else {
        alarm = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->alarm_column)->property("Value").toString();
        if (alarm == "++") {
            type = "\"critical_alarm\"";
        } else if (alarm == "+") {
            type = "\"alarm\"";
        } else {
            if ((type_value == "DI") || (type_value == "DO")) {
                type = "\"bool\"";
                signal_type_value = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->signal_type_column)->property("Value").toString();
            } else {
                emit debug_output("\tНевозможно определить тип сигнала с индексом " + index + "\n", RED);
                return CONVERTER_ERROR;
            }
        }
    }

    /* Определяем имя */
    system_id = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->system_id_column)->property("Value").toString();
    signal_id = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->signal_id_column)->property("Value").toString();
    name = "\"" + system_id + "_" + signal_id + "\"";

    find_range = excel_sheet_alg->querySubObject("Columns(string)", "B:B")->querySubObject("Find(string)", system_id + "_" + signal_id);
    if (find_range != nullptr) {
        if (find_range->property("Value").toString() == system_id + "_" + signal_id) {
            emit debug_output("\tСигнал " + index + "(" + system_id + "_" + signal_id +")" + " повторно определен в алгоритмах\n", RED);
            return CONVERTER_ERROR;
        }
    }

    /* Определяем описание */
    description = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->param_name_column)->property("Value").toString();
    description = "\"" + description.remove("\"", Qt::CaseInsensitive);
    description += "\"";

    /* Пишем строку */
    if (inverted == "\"true\"") {
        result_string = "\t\t<parm name=" + name + " type=" + type + " inverted=" + inverted + " description=" + description + "/>";
    } else {
        result_string = "\t\t<parm name=" + name + " type=" + type +  " description=" + description + "/>";
    }
    xml_stream << result_string << endl;

    if (type_value == "AI") {
        WriteSetpointStrings(navigation, parameter_row, (system_id + "_" + signal_id));
    }

    return CONVERTER_OK;
}

unsigned int XlsxConverter::FillNscDevAlg(struct navigation_struct * navigation, QString shmem) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    QString name;
    QString type;
    QString inverted;
    QString script;
    QString description;
    for (int i = 2; i <= rows_count_sheet_alg; ++i) {
        if (excel_sheet_alg->querySubObject("Cells(int,int)", i, 1)->property("Value").toString() == shmem) {
            name = excel_sheet_alg->querySubObject("Cells(int,int)", i, 2)->property("Value").toString();
            type = excel_sheet_alg->querySubObject("Cells(int,int)", i, 3)->property("Value").toString();
            script = excel_sheet_alg->querySubObject("Cells(int,int)", i, 4)->property("Value").toString();
            description = excel_sheet_alg->querySubObject("Cells(int,int)", i, 5)->property("Value").toString();
            result_string = "\t\t<parm name=\"" + name +"\" type=\"" + type +"\" description=\"" + description +"\" script=\"" + script +"\"/>";
            xml_stream << result_string <<endl;
        }
    }
}

/* Генерация блока с адресами сигналов */
unsigned int XlsxConverter::GenerateAdresses(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    QString result_string;
    QString read_value;
    int pvv_num = 0;       // Номер ПВВ
    int pvv_column = 1;    // Столбец с названиями ПВВ в раскладке
    int pvv_row;           // Строка на листе ракладки

    emit debug_output("Генерируем блоки с адресами:\n", BLACK);

    /* Генерируем блок для указанных ПВВ (должно стоять $$_*номер ПВВ) */
    for (pvv_row = 1; pvv_row <= rows_count_sheet_2; ++pvv_row) {
        /* Читаем значение в ячейке */
        read_value = excel_sheet_2->querySubObject("Cells(int,int)", pvv_row, pvv_column)->property("Value").toString();
        /* Определяем, нужно ли генерировать сигналы для этого ПВВ */
        if(read_value.indexOf("$$_") != -1) {
            pvv_num = read_value[read_value.indexOf("$$_") + 3].digitValue();
            emit debug_output("\tПВВ №" + QString::number(pvv_num) + ":\n", BLACK);
            FillAdressBlock(navigation, pvv_num, pvv_row + 2, pvv_column + 1);
        }
    }
    return  0;
}

unsigned int XlsxConverter::FillAdressBlock(struct navigation_struct * navigation, unsigned int pvv_num,
                                          unsigned int first_signal_row, unsigned int first_signal_column) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    QString ip_con1;
    QString ip_con2;
    QAxObject* find_range;
    QString device_name;
    QString signals_type;
    QString signal_index;
    int signal_address = 1;
    int adress_row;
    int module_count;
    int modbus_function;
    int signal_number;

    /* Пишем заголовок для блока с параметрами связи */
    result_string = "\t<device name=\"DIO_" + QString::number(pvv_num) + "\" protocol=\"MODBUS-TCP\" check_timeout_min=\"1\">";
    xml_stream << result_string <<endl;

    /* Определяем IP-адреса устройства */
    device_name = "ПСТС" + QString::number(pvv_num);
    find_range = excel_sheet_7->querySubObject("Columns(string)", "A:A")->querySubObject("Find(string)", device_name);
    if (find_range != nullptr) {
        adress_row = find_range->property("Row").toInt();
    } else {
        emit debug_output("\tНа странице IP нет информации о " + device_name + "\n", RED);
        return CONVERTER_ERROR;
    }
    ip_con1 = excel_sheet_7->querySubObject("Cells(int,int)", adress_row, 2)->property("Value").toString();
    ip_con2 = excel_sheet_7->querySubObject("Cells(int,int)", adress_row, 3)->property("Value").toString();
    /* Выводим данные о подключении */
    result_string = "\t\t<connection host=\"" + ip_con1 + "\" port=\"502\" parm=\"DIO_" + QString::number(pvv_num) + "_con1\"/>";
    xml_stream << result_string <<endl;
    result_string = "\t\t<connection host=\"" + ip_con2 + "\" port=\"502\" parm=\"DIO_" + QString::number(pvv_num) + "_con2\"/>";
    xml_stream << result_string <<endl<<endl;

    /* Пишем адреса */
    for (module_count = 0; module_count < 8; ++module_count) {
        signals_type = excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row - 2, first_signal_column + module_count)->property("Value").toString();
        if (signals_type.contains("DO")) {
            modbus_function = 15;
        } else if (signals_type.contains("DI")) {
             modbus_function = 2;
        } else if (signals_type.contains("AO")) {
             modbus_function = 3;
        } else {
            modbus_function = 4;
        }
            for (signal_number = 0; signal_number < 32; ++signal_number) {
                signal_index =excel_sheet_2->querySubObject("Cells(int,int)", first_signal_row + signal_number, first_signal_column + module_count)->property("Value").toString();
                if(!signal_index.isEmpty()) {
                    FillSignalAdress(navigation, signal_index, modbus_function, signal_address);
                }
                ++signal_address;
            }
    }
    FillAddrAlg(navigation, QString::number(pvv_num));
    result_string = "\t</device>";
    xml_stream << result_string <<endl<<endl;
    return CONVERTER_OK;
}

unsigned int XlsxConverter::FillAddrAlg(struct navigation_struct * navigation, QString pvv) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    QString shmem;
    QString name;
    QString func;
    QString addr;
    QAxObject* find_range;
    for (int i = 2; i <= rows_count_sheet_alg; ++i) {
        shmem = excel_sheet_alg->querySubObject("Cells(int,int)", i, 1)->property("Value").toString();
        if (shmem.contains("dev" + pvv)) {
            addr = excel_sheet_alg->querySubObject("Cells(int,int)", i, 6)->property("Value").toString();
            if (!addr.isEmpty()) {
                func = excel_sheet_alg->querySubObject("Cells(int,int)", i, 7)->property("Value").toString();
                name = excel_sheet_alg->querySubObject("Cells(int,int)", i, 2)->property("Value").toString();
                result_string = "\t\t<address start_addr=\"" + addr + "\" type=\"" + func + "\" parm=\"" + name + "\"/>";
                xml_stream << result_string <<endl;
            }
        }
    }
    return CONVERTER_OK;
}

unsigned int XlsxConverter::FillSignalAdress(struct navigation_struct * navigation, QString index, int modbus_function, int signal_address) {
    QTextStream xml_stream(xml_file);
    QVariant index_var = index;
    xml_stream.setCodec("UTF-8");
    QAxObject* find_range;
    if ((modbus_function != 1) && (modbus_function != 2) && (modbus_function != 15)) {
        signal_address = (signal_address % 32) + (signal_address / 32) * 16;
    }
    QString address = QString::number(signal_address);
    QString function;
    if (modbus_function <= 15) {
        function = "0x0" + QString::number(modbus_function, 16);
    } else {
        function = "0x" + QString::number(modbus_function, 16);
    }
    QString signal_id;
    QString system_id;
    QString name;
    QString result_string;
    int parameter_row;

    /* Ищем индекс в перечне */
    find_range = excel_sheet_1->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", index);
    if (find_range != nullptr) {
        parameter_row = find_range->property("Row").toInt();
    } else {
        emit debug_output("\tСигнал с индексом " + index + " отсутствует в перечне!\n", RED);
        return CONVERTER_ERROR;
    }

    /* Определяем имя */
    system_id = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->system_id_column)->property("Value").toString();
    signal_id = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, navigation->signal_id_column)->property("Value").toString();
    name = "\"" + system_id + "_" + signal_id + "\"";

    find_range = excel_sheet_alg->querySubObject("Columns(string)", "B:B")->querySubObject("Find(string)", system_id + "_" + signal_id);
    if (find_range != nullptr) {
        if (find_range->property("Value").toString() == system_id + "_" + signal_id) {
            emit debug_output("\tСигнал " + index + "(" + system_id + "_" + signal_id +")" + " повторно определен в алгоритмах\n", RED);
            return CONVERTER_ERROR;
        }
    }
    result_string = "\t\t<address start_addr=\"" + address + "\" type=\"" + function + "\" parm=" + name + "/>";
    xml_stream << result_string <<endl;

    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateConnections(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    unsigned int row_count;
    QString name;
    emit debug_output("Генерируем блок nsc-dev-connection...", BLACK);

    /* Пишем заголовок для блока с параметрами связи */
    result_string = "\t<shmem name=\"nsc-dev-connection\" stale_timeout=\"120000\">";
    xml_stream << result_string <<endl;

    /* Заполняем */
    for (row_count = 2; row_count <= rows_count_sheet_7; ++row_count) {
        name = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
        if (name != nullptr) {
            result_string = "\t\t<parm name=\"" + name +"\" type=\"int\"/>";
            xml_stream << result_string <<endl;
        }
    }

    /* Закрываем блок */
    result_string = "\t</shmem>";
    xml_stream << result_string <<endl<<endl;
    emit debug_output("Успешно! \n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateWorkstations(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    unsigned int row_count = 3;
    QString name = "default";
    QString host1, host2;
    QString priority;

    emit debug_output("Генерируем блоки workstation...", BLACK);
    name = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
    while (name != nullptr) {
        priority = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 5)->property("Value").toString();
        host1 = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 2)->property("Value").toString();
        host2 = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 3)->property("Value").toString();
        if (priority != nullptr) {
            result_string = "\t<workstation name=\"" + name + "\" priority=\"" + priority + "\">";
        } else {
            result_string = "\t<workstation name=\"" + name + "\">";
        }
        xml_stream << result_string <<endl;
        result_string = "\t\t <connection host=\"" + host1 + "\" port=\"50201\"/>";
        xml_stream << result_string <<endl;
        result_string = "\t\t <connection host=\"" + host2 + "\" port=\"50201\"/>";
        xml_stream << result_string <<endl;
        result_string = "\t</workstation>";
        xml_stream << result_string <<endl<<endl;
        ++row_count;
        name = excel_sheet_7->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
    }
    emit debug_output("Успешно! \n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::CheckParams(struct navigation_struct * navigation) {
    QString param;
    QString bus_state;
    QAxObject*  find_range;
    int counter;
    emit debug_output("Проверка раскладки предмет отсутствующих сигналов:\n", BLACK);
    for (counter = 3; counter <= rows_count_sheet_1; ++counter) {
         param = excel_sheet_1->querySubObject("Cells(int,int)", counter, navigation->param_index_column)->property("Value").toString();
         if (param != nullptr) {
             bus_state = excel_sheet_1->querySubObject("Cells(int,int)", counter, navigation->bus_column)->property("Value").toString();
             if (bus_state == nullptr) {
                 find_range = excel_sheet_2->querySubObject("Columns(string)", "A:I")->querySubObject("Find(string)", param);
                 if(find_range == nullptr) {
                      emit debug_output("Сигнал "+ param + " отсутствует в раскладке\n", RED);
                 }
             }
         }
    }
    emit debug_output("Готово\n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GeneratePus(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QAxObject*  find_range;
    QString result_string;
    QString param_name = "default";
    QString type;
    QString shmem;
    QString script;
    QString description;
    QString start_addr;
    QString function;
    QString host1, host2;
    int adress_row = 0;
    int row_count = 0;

    find_range = excel_sheet_7->querySubObject("Columns(string)", "A:A")->querySubObject("Find(string)", "ПУС");
    if (find_range == nullptr) {
        emit debug_output("ПУС не найден среди приборов сопряжения!\n", RED);
        return CONVERTER_ERROR;
    }
    adress_row = find_range->property("Row").toInt();
    host1 = excel_sheet_7->querySubObject("Cells(int,int)", adress_row, 2)->property("Value").toString();
    host2 = excel_sheet_7->querySubObject("Cells(int,int)", adress_row, 3)->property("Value").toString();

    /* Генерируем заголовок PUS */
    emit debug_output("Генерируем блок device PUS...", BLACK);
    result_string = "\t<device name=\"PUS\" protocol=\"MODBUS-TCP\" check_timeout_min=\"1\" write_timeout_ms=\"500\">";
    xml_stream << result_string <<endl;
    result_string = "\t\t<connection host=\"" + host1 +"\" port=\"502\" parm=\"PUS_con1\"/>";
    xml_stream << result_string <<endl;
    result_string = "\t\t<connection host=\"" + host2 +"\" port=\"502\" parm=\"PUS_con2\"/>";
    xml_stream << result_string <<endl<<endl;

    for (row_count = 2; row_count <= rows_count_sheet_pus; ++row_count) {
        start_addr = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 6)->property("Value").toString();
        if (start_addr != nullptr) {
            function = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 7)->property("Value").toString();
            param_name = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 1)->property("Value").toString();
            result_string = "\t\t<address start_addr=\"" + start_addr + "\" type=\"" + function + "\" parm=\"" + param_name + "\"/>";
            xml_stream << result_string <<endl;
        }
    }
    result_string = "\t</device>";
    xml_stream << result_string <<endl <<endl;
    emit debug_output("Готово\n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::FillPus(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QAxObject*  find_range;
    QString result_string;
    QString param_name = "default";
    QString type;
    QString shmem;
    QString script;
    QString description;
    QString start_addr;
    QString function;
    int row_count = 0;

    emit debug_output("Генерируем smem'ы для ПУС...", BLACK);
    /* Определяем, есть ли DO и создаем блок для него */
    find_range = excel_sheet_pus->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", "nsc-pus-DO");
    if (find_range != nullptr) {
        /* Генерируем заголовок */
        result_string = "\t<shmem name=\"nsc-pus-DO\" stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (row_count = 2; row_count <= rows_count_sheet_pus; ++row_count) {
            shmem = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 3)->property("Value").toString();
            if (shmem == "nsc-pus-DO") {
                param_name = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 1)->property("Value").toString();
                type = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 2)->property("Value").toString();
                script = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
                description = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 5)->property("Value").toString();
                if (script != "")
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" script=\"" + script + "\" description=\"" + description +"\"/>";
                else
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" description=\"" + description +"\"/>";
                xml_stream << result_string <<endl;
            }
        }
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl <<endl;
    }
    find_range = excel_sheet_pus->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", "nsc-pus-DI");
    if (find_range != nullptr) {
        /* Генерируем заголовок */
        result_string = "\t<shmem name=\"nsc-pus-DI\" stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (row_count = 2; row_count <= rows_count_sheet_pus; ++row_count) {
            shmem = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 3)->property("Value").toString();
            if (shmem == "nsc-pus-DI") {
                param_name = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 1)->property("Value").toString();
                type = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 2)->property("Value").toString();
                script = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
                description = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 5)->property("Value").toString();
                if (script != "")
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" script=\"" + script + "\" description=\"" + description +"\"/>";
                else
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" description=\"" + description +"\"/>";
                xml_stream << result_string <<endl;
            }
        }
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl <<endl;
    }
    find_range = excel_sheet_pus->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", "nsc-pus-AI");
    if (find_range != nullptr) {
        /* Генерируем заголовок */
        result_string = "\t<shmem name=\"nsc-pus-AI\" stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (row_count = 2; row_count <= rows_count_sheet_pus; ++row_count) {
            shmem = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 3)->property("Value").toString();
            if (shmem == "nsc-pus-AI") {
                param_name = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 1)->property("Value").toString();
                type = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 2)->property("Value").toString();
                script = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
                description = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 5)->property("Value").toString();
                if (script != "")
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" script=\"" + script + "\" description=\"" + description +"\"/>";
                else
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" description=\"" + description +"\"/>";
                xml_stream << result_string <<endl;
            }
        }
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl <<endl;
    }
    find_range = excel_sheet_pus->querySubObject("Columns(string)", "C:C")->querySubObject("Find(string)", "nsc-pus-AO");
    if (find_range != nullptr) {
        /* Генерируем заголовок */
        result_string = "\t<shmem name=\"nsc-pus-AO\" stale_timeout=\"45000\">";
        xml_stream << result_string <<endl;
        for (row_count = 2; row_count <= rows_count_sheet_pus; ++row_count) {
            shmem = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 3)->property("Value").toString();
            if (shmem == "nsc-pus-AO") {
                param_name = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 1)->property("Value").toString();
                type = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 2)->property("Value").toString();
                script = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 4)->property("Value").toString();
                description = excel_sheet_pus->querySubObject("Cells(int,int)", row_count, 5)->property("Value").toString();
                if (script != "")
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" script=\"" + script + "\" description=\"" + description +"\"/>";
                else
                    result_string = "\t\t<parm name=\"" + param_name + "\" type=\"" + type + "\" description=\"" + description +"\"/>";
                xml_stream << result_string <<endl;
            }
        }
        result_string = "\t</shmem>";
        xml_stream << result_string <<endl <<endl;
    }
    emit debug_output("Успешно!\n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::WriteSetpointStrings(struct navigation_struct * navigation, int parameter_row, QString param_name) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");

    QString  type;
    QString script;
    QString description;
    QString setpoint_description;
    QString setponts_types;
    QString result_string;
    QString measure;
    QStringList setpoints_list;
    QStringList values_list;
    QString values;
    QString setpoint;
    QString setpoint_param_name;

    setponts_types = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, 8)->property("Value").toString();
    setponts_types = setponts_types.replace(" ", "");
    description = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, 4)->property("Value").toString();
    values =  excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, 9)->property("Value").toString();
    values = values.replace(" ", "");
    measure = excel_sheet_1->querySubObject("Cells(int,int)", parameter_row, 7)->property("Value").toString();
    result_string = "\t\t<parm name=\"" + param_name +"_uni\" type=\"float\" script=\"alg.interpolation(" + param_name + ", x0_f8017c1, y1_" + param_name + ")\" description=\"" + description + " в "+ measure+"\"/>";
    xml_stream << result_string <<endl;
    result_string = "\t\t<parm name=\"" + param_name +"_fault\" type=\"alarm\" script=\"(" + param_name + " &lt; 0) || (" + param_name + " &gt; 10000.0)\" description=\"Отказ датчика " + description +"\"/>";
    xml_stream << result_string <<endl;
    setpoints_list =  setponts_types.split(QRegExp(","), QString::SkipEmptyParts);
    values_list = values.split(QRegExp(","), QString::SkipEmptyParts);
    if (setpoints_list.length() != values_list.length()) {
        emit debug_output("Неверно заданы уставки для параметра " + param_name + "\n", RED);
        return CONVERTER_ERROR;
    }

    for (int i = 0; i < setpoints_list.length(); ++i) {
        if (setpoints_list[i] == "LL") {
            type = "critical_alarm";
            setpoint_description = "АНУ " + description;
            setpoint_param_name = param_name + "_LL";
            script = param_name + "_uni &lt; " + values_list[i];
        }
        if (setpoints_list[i] == "L") {
            type = "alarm";
            setpoint_description = "АНУ " + description;
            setpoint_param_name = param_name + "_L";
            script = param_name + "_uni &lt; " + values_list[i];
        }
        if (setpoints_list[i] == "HH") {
            type = "critical_alarm";
            setpoint_description = "ВУ " + description;
            setpoint_param_name = param_name + "_HH";
            script = param_name + "_uni &gt; " + values_list[i];
        }
        if (setpoints_list[i] == "H") {
            type = "alarm";
            setpoint_description = "ВУ " + description;
            setpoint_param_name = param_name + "_H";
            script = param_name + "_uni &gt; " + values_list[i];
        }
        result_string = "\t\t<parm name=\"" + setpoint_param_name +"\" type=\""+ type +"\" script=\""+script+"\" description=\""+setpoint_description+"\"/>";
        if (type != "dontcreate") {
            xml_stream << result_string <<endl;
            type = "dontcreate";
        }
    }

    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateAlgstatic(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    QString type;
    QString description;
    QString script;
    QString name;

    emit debug_output("Генерируем блок nsc-dev-algstatic...", BLACK);
    result_string = "\t<shmem name=\"nsc-dev-algstatic\" keepalive=\"true\">";
    xml_stream << result_string <<endl;

    FillNscDevAlg(navigation, "nsc-dev-algstatic");
    xml_stream  << endl;

    GenerateUPS(navigation);
    GenerateSK(navigation);
    GenerateVK(navigation);
    GenerateKDMP(navigation);

    result_string = "\t</shmem>";
    xml_stream << result_string <<endl << endl;
    emit debug_output("Успешно!\n", GREEN);
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateUPS(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QAxObject*  find_range;
    QString result_string;
    QString script = "";
    QString ups_id;
    QString signal_ups;
    QString adress_row;
    int adress_row_int;
    QString ups_find_range;
    QString param_name;
    QString setponts_types;
    QStringList setpoints_list;

    // Для каждого УПС
    for (int i = 2; i <= rows_count_sheet_ups; ++i) {
        ups_find_range = "AG2:AG" + QString::number(rows_count_sheet_1);
        ups_id = excel_sheet_ups->querySubObject("Cells(int,int)", i, 1)->property("Value").toString();
        result_string = "\t\t<parm name=\"UPS_" + ups_id + "\" type=\"int\" script=\"";
        find_range = find_range = excel_sheet_1->querySubObject("Range(string)", ups_find_range)->querySubObject("Find(string)", ups_id);
        while (find_range != nullptr) {
            param_name = "";
            adress_row = find_range->property("Row").toString();
            adress_row_int = adress_row.toInt();
            if (find_range->property("Value").toString() == ups_id) {

                param_name = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->system_id_column)->property("Value").toString() +
                            "_" + excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->signal_id_column)->property("Value").toString();
                result_string += param_name;
                if (excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->type_column)->property("Value").toString() == "AI")  {
                    result_string += "_fault";
                    setponts_types = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int, 8)->property("Value").toString();
                    setponts_types = setponts_types.replace(" ", "");
                    setpoints_list =  setponts_types.split(QRegExp(","), QString::SkipEmptyParts);
                    for (int i = 0; i < setpoints_list.length(); ++i) {
                        if (setpoints_list[i] == "LL") {
                            result_string += " || " + param_name + "_LL";
                        }
                        if (setpoints_list[i] == "L") {
                            result_string += " || " + param_name + "_L";
                        }
                        if (setpoints_list[i] == "HH") {
                            result_string += " || " + param_name + "_HH";
                        }
                        if (setpoints_list[i] == "H") {
                            result_string += " || " + param_name + "_H";
                        }
                    }
                }
            }
            ups_find_range = "AG" + QString::number(adress_row_int) + ":AG" + QString::number(rows_count_sheet_1);
            find_range = excel_sheet_1->querySubObject("Range(string)", ups_find_range)->querySubObject("Find(string)", ups_id);
            if (find_range->property("Row").toString() == adress_row) {
                ++adress_row_int;
                ups_find_range = "AG" + QString::number(adress_row_int) + ":AG" + QString::number(rows_count_sheet_1);
                find_range = excel_sheet_1->querySubObject("Range(string)", ups_find_range)->querySubObject("Find(string)", ups_id);
            }
            if ((find_range != nullptr) && (param_name != "") && (find_range->property("Value").toString() == ups_id)) {
                result_string += " || ";
            }

        }
        result_string += "\" description=\"" + excel_sheet_ups->querySubObject("Cells(int,int)", i, 2)->property("Value").toString() + "\"/>";
        xml_stream << result_string << endl;
    }
    xml_stream << endl;
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateSK(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QString result_string;
    QString sk_windows[rows_count_sheet_ups];
    QString sk_num;
    QString script;
    QString description;

    for (int i = 2; i <= rows_count_sheet_ups; ++i) {
        sk_num = excel_sheet_ups->querySubObject("Cells(int,int)", i , 3)->property("Value").toString();
        if (sk_num != "") {
            sk_windows[sk_num.toInt()] += excel_sheet_ups->querySubObject("Cells(int,int)", i , 1)->property("Value").toString() + ",";
        }
    }
    for (int i = 0; i < rows_count_sheet_ups; ++i) {
        script = "";
        if (sk_windows[i] != nullptr) {
            QString ups_string = sk_windows[i];
            QStringList ups_nums = ups_string.split(QRegExp(","), QString::SkipEmptyParts);
            for (int j = 0; j < ups_nums.length(); ++j) {
                if (j != 0) {
                    script += " || ";
                }
                script += "UPS_" + ups_nums[j];
            }
            description = excel_sheet_ups->querySubObject("Cells(int,int)", i , 4)->property("Value").toString();
            result_string = "\t\t<parm name=\"SK_" + QString::number(i) + "\" type=\"int\" script=\"" + script + "\" description=\"" + description+ "\"/>";
            xml_stream << result_string << endl;
        }
    }
    xml_stream  << endl;
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateVK(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QAxObject*  find_range;
    QString result_string;
    QString script = "";
    QString vk_id;
    QString signal_vk;
    QString adress_row;
    int adress_row_int;
    QString vk_find_range;
    QString param_name;
    QString setponts_types;
    QStringList setpoints_list;
    QString vk_name;
    QString vk_description;
    QString signalisation;
    int elemnts_count = 0;

    for (int i = 2; i <= rows_count_sheet_vk; ++i) {
        elemnts_count = 0;
        vk_find_range = "AI2:AI" + QString::number(rows_count_sheet_1);
        vk_id = excel_sheet_vk->querySubObject("Cells(int,int)", i, 1)->property("Value").toString();
        vk_name = excel_sheet_vk->querySubObject("Cells(int,int)", i, 3)->property("Value").toString();
        vk_description = excel_sheet_vk->querySubObject("Cells(int,int)", i, 2)->property("Value").toString();
        result_string = "\t\t<parm name=\"" + vk_name + "\" type=\"int\" script=\"";
        find_range = find_range = excel_sheet_1->querySubObject("Range(string)", vk_find_range)->querySubObject("Find(string)", vk_id);
        while (find_range != nullptr) {
            param_name = "";
            adress_row = find_range->property("Row").toString();
            adress_row_int = adress_row.toInt();
            signalisation = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , 12)->property("Value").toString();

            param_name = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->system_id_column)->property("Value").toString() +
                        "_" + excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->signal_id_column)->property("Value").toString();

            if ((find_range->property("Value").toString() == vk_id) && ((signalisation == "+") || (signalisation == "++"))) {
                ++elemnts_count;
                result_string += param_name;
                if (excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->type_column)->property("Value").toString() == "AI")  {
                    result_string += "_fault";
                    setponts_types = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int, 8)->property("Value").toString();
                    setponts_types = setponts_types.replace(" ", "");
                    setpoints_list =  setponts_types.split(QRegExp(","), QString::SkipEmptyParts);
                    for (int i = 0; i < setpoints_list.length(); ++i) {
                        if (setpoints_list[i] == "LL") {
                            result_string += " || " + param_name + "_LL";
                        }
                        if (setpoints_list[i] == "L") {
                            result_string += " || " + param_name + "_L";
                        }
                        if (setpoints_list[i] == "HH") {
                            result_string += " || " + param_name + "_HH";
                        }
                        if (setpoints_list[i] == "H") {
                            result_string += " || " + param_name + "_H";
                        }
                    }
                }
            }
            vk_find_range = "AI" + QString::number(adress_row_int) + ":AI" + QString::number(rows_count_sheet_1);
            find_range = excel_sheet_1->querySubObject("Range(string)", vk_find_range)->querySubObject("Find(string)", vk_id);
            if (find_range->property("Row").toString() == adress_row) {
                ++adress_row_int;
                vk_find_range = "AI" + QString::number(adress_row_int) + ":AI" + QString::number(rows_count_sheet_1);
                find_range = excel_sheet_1->querySubObject("Range(string)", vk_find_range)->querySubObject("Find(string)", vk_id);
            }
            if (find_range != nullptr) {
                adress_row = find_range->property("Row").toString();
                adress_row_int = adress_row.toInt();
                signalisation = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , 12)->property("Value").toString();
            } else {
                signalisation = "";
            }
            if ((find_range != nullptr) && (param_name != "") && (find_range->property("Value").toString() == vk_id) &&
                    ((signalisation == "+") || (signalisation == "++")) && (elemnts_count > 0)) {
                result_string += " || ";
            }

        }
        result_string += "\" description=\"" + vk_description + "\"/>";
        xml_stream << result_string << endl;
    }
    xml_stream << endl;
    return CONVERTER_OK;
}

unsigned int XlsxConverter::GenerateKDMP(struct navigation_struct * navigation) {
    QTextStream xml_stream(xml_file);
    xml_stream.setCodec("UTF-8");
    QAxObject*  find_range;
    QString result_string;
    QString script = "";
    QString adress_row;
    int adress_row_int;
    QString kdmp_find_range;
    QString param_name;
    QString setponts_types;
    QStringList setpoints_list;
    QString kdmp;

        kdmp_find_range = "AH2:AH" + QString::number(rows_count_sheet_1);
        result_string = "\t\t<parm name=\"kdmp_auto_on\" type=\"bool\" script=\"get_alarm(";
        find_range = find_range = excel_sheet_1->querySubObject("Range(string)", kdmp_find_range)->querySubObject("Find(string)", "+");
        while (find_range != nullptr) {
            param_name = "";
            adress_row = find_range->property("Row").toString();
            adress_row_int = adress_row.toInt();
            if (find_range->property("Value").toString() == "+") {

                param_name = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->system_id_column)->property("Value").toString() +
                            "_" + excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->signal_id_column)->property("Value").toString();
                result_string += param_name;
                if (excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int , navigation->type_column)->property("Value").toString() == "AI")  {
                    result_string += "_fault";
                    setponts_types = excel_sheet_1->querySubObject("Cells(int,int)", adress_row_int, 8)->property("Value").toString();
                    setponts_types = setponts_types.replace(" ", "");
                    setpoints_list =  setponts_types.split(QRegExp(","), QString::SkipEmptyParts);
                    for (int i = 0; i < setpoints_list.length(); ++i) {
                        if (setpoints_list[i] == "LL") {
                            result_string += " || " + param_name + "_LL";
                        }
                        if (setpoints_list[i] == "L") {
                            result_string += " || " + param_name + "_L";
                        }
                        if (setpoints_list[i] == "HH") {
                            result_string += " || " + param_name + "_HH";
                        }
                        if (setpoints_list[i] == "H") {
                            result_string += " || " + param_name + "_H";
                        }
                    }
                }
            }
            kdmp_find_range = "AH" + QString::number(adress_row_int) + ":AH" + QString::number(rows_count_sheet_1);
            find_range = excel_sheet_1->querySubObject("Range(string)", kdmp_find_range)->querySubObject("Find(string)", "+");
            if (find_range->property("Row").toString() == adress_row) {
                ++adress_row_int;
                kdmp_find_range = "AH" + QString::number(adress_row_int) + ":AH" + QString::number(rows_count_sheet_1);
                find_range = excel_sheet_1->querySubObject("Range(string)", kdmp_find_range)->querySubObject("Find(string)", "+");
            }
            if ((find_range != nullptr) && (param_name != "") && (find_range->property("Value").toString() == "+")) {
                result_string += " || ";
            }

        }
        result_string += ")\" description=\"Автовключение КДМП по параметру АПС\"/>";
        xml_stream << result_string << endl;
    xml_stream << endl;
    return CONVERTER_OK;
}
