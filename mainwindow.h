#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "xlsxconverter.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString xlsx_path;
    XlsxConverter converter;
    void start_convert(QString path);


private slots:
    void on_toolButton_clicked();
    void on_enterButton_clicked();
    void on_selectEdit_textChanged(const QString &arg1);

public slots:
    void append_debug_console(QString str, QString color);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
