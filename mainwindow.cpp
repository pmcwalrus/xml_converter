#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QAxObject>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(size());             // Запрещаем менять размер окна
    ui->outputText->setReadOnly(true);      // Запрещаем редактировать текст в области вывода

    /* Если путь пустой, деактивируем кнопку запуска*/
    if (ui->selectEdit->text().isEmpty()) {
        ui->enterButton->setDisabled(true);
    } else {
        ui->enterButton->setDisabled(false);
    }

    QObject::connect(&converter, SIGNAL(debug_output(QString, QString)), this, SLOT(append_debug_console(QString, QString)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Обработка нажатия кнопки выбора файла */
void MainWindow::on_toolButton_clicked()
{
   /* Открываем окно выбора файла */
   xlsx_path  = QFileDialog::getOpenFileName(this, "Выбор таблицы сигналов", "", "*.xlsx *.xls");
   /* Копируем путь к файлу в строку выбора */
   ui->selectEdit->setText(xlsx_path);
}

/* Обработка нажатия кнопки Enter */
void MainWindow::on_enterButton_clicked()
{
    /* Начинаем обраотку фала */
    start_convert(xlsx_path);
}

/* Обработка изменения пути в текстовом поле */
void MainWindow::on_selectEdit_textChanged(const QString &arg1)
{
    /* Если путь не пустой, активируем кнопку запуска*/
    if (arg1.isEmpty()) {
        ui->enterButton->setDisabled(true);
    } else {
        ui->enterButton->setDisabled(false);
    }
    /* Задаем путь к файлу */
    xlsx_path = arg1;
}

void MainWindow:: append_debug_console(QString str, QString color) {
    ui->outputText->moveCursor(QTextCursor::End);
    ui->outputText->setTextColor(color);
    ui->outputText->insertPlainText(str);
    ui->outputText->repaint();
}

void MainWindow::start_convert(QString path) {
    converter.ConvertXlsxToXml(path);
}
